# Strapi data services et profile info

## Prérequis 

Installer node et utiliser docker pour la collection MongDB

*  [docker](https://docs.docker.com/)
*  [node](https://nodejs.org/en/)

Lancer le deamon docker :

`docker run -rm -d -p 27017:27107/tcp -v /data:/data/db mongo:4`

## Installer dependance & Start

`npm install strapi@alpha -g`

`npm install`

`strapi start`

Dans le navigateur les route disponible sont : 

[http://localhost:1337/admin/](http://localhost:1337/admin/)-> Dashboard admin

[http://localhost:1337/admin/plugins/upload/configurations/development](http://localhost:1337/admin/plugins/upload/configurations/development)-> provider Cloudinary

[https://localhost:1337/auth/local](https://localhost:1337/auth/local) -> POST Authentification (jwt)


## Heroku deploy

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

`$ heroku login`

`$ heroku git:remote -a strapi-conciergerie`

```
$ git add .
$ git commit -am "make it better"
```

`$ heroku addons:create mongolab:sandbox --as DATABASE`

`$ heroku config`

Set environnement variable in heroku :

`$ heroku config:set EXEMPLE=exemple`

DATABASE_URI
DATABASE_HOST
DATABASE_PORT
DATABASE_AUTHENTICATION_DATABASE
DATABASE_NAME
DATABASE_USERNAME
DATABASE_PASSWORD

`$ git push heroku master`


## Author & Licences

Adrien Gillmann
