'use strict';

/**
 * Ville.js controller
 *
 * @description: A set of functions called "actions" for managing `Ville`.
 */

module.exports = {

  /**
   * Retrieve ville records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.ville.search(ctx.query);
    } else {
      return strapi.services.ville.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a ville record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.ville.fetch(ctx.params);
  },

  /**
   * Count ville records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.ville.count(ctx.query);
  },

  /**
   * Create a/an ville record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.ville.add(ctx.request.body);
  },

  /**
   * Update a/an ville record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.ville.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an ville record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.ville.remove(ctx.params);
  }
};
