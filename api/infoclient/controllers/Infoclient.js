'use strict';

/**
 * Infoclient.js controller
 *
 * @description: A set of functions called "actions" for managing `Infoclient`.
 */

module.exports = {

  /**
   * Retrieve infoclient records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.infoclient.search(ctx.query);
    } else {
      return strapi.services.infoclient.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a infoclient record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.infoclient.fetch(ctx.params);
  },

  /**
   * Count infoclient records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.infoclient.count(ctx.query);
  },

  /**
   * Create a/an infoclient record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.infoclient.add(ctx.request.body);
  },

  /**
   * Update a/an infoclient record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.infoclient.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an infoclient record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.infoclient.remove(ctx.params);
  }
};
