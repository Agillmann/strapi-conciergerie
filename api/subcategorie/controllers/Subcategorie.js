'use strict';

/**
 * Subcategorie.js controller
 *
 * @description: A set of functions called "actions" for managing `Subcategorie`.
 */

module.exports = {

  /**
   * Retrieve subcategorie records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.subcategorie.search(ctx.query);
    } else {
      return strapi.services.subcategorie.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a subcategorie record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.subcategorie.fetch(ctx.params);
  },

  /**
   * Count subcategorie records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.subcategorie.count(ctx.query);
  },

  /**
   * Create a/an subcategorie record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.subcategorie.add(ctx.request.body);
  },

  /**
   * Update a/an subcategorie record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.subcategorie.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an subcategorie record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.subcategorie.remove(ctx.params);
  }
};
