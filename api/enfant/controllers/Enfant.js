'use strict';

/**
 * Enfant.js controller
 *
 * @description: A set of functions called "actions" for managing `Enfant`.
 */

module.exports = {

  /**
   * Retrieve enfant records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.enfant.search(ctx.query);
    } else {
      return strapi.services.enfant.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a enfant record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.enfant.fetch(ctx.params);
  },

  /**
   * Count enfant records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.enfant.count(ctx.query);
  },

  /**
   * Create a/an enfant record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.enfant.add(ctx.request.body);
  },

  /**
   * Update a/an enfant record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.enfant.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an enfant record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.enfant.remove(ctx.params);
  }
};
