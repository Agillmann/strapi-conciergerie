'use strict';

/**
 * Contactsupp.js controller
 *
 * @description: A set of functions called "actions" for managing `Contactsupp`.
 */

module.exports = {

  /**
   * Retrieve contactsupp records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.contactsupp.search(ctx.query);
    } else {
      return strapi.services.contactsupp.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a contactsupp record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.contactsupp.fetch(ctx.params);
  },

  /**
   * Count contactsupp records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.contactsupp.count(ctx.query);
  },

  /**
   * Create a/an contactsupp record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.contactsupp.add(ctx.request.body);
  },

  /**
   * Update a/an contactsupp record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.contactsupp.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an contactsupp record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.contactsupp.remove(ctx.params);
  }
};
